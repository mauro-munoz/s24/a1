

db.users.insertMany([
{
    "firstName" : "Jane",
    "lastName" : "Doe",
    "age" : 21.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "janedoe@gmail.com"
    },
    "courses" : [ 
        "CSS", 
        "Javascript", 
        "Python"
    ],
    "department" : "HR",
    "isAdmin" : false,
    "status" : "active"
},
{
    "firstName" : "Stephen",
    "lastName" : "Hawking",
    "age" : 76.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "stephenhawking@gmail.com"
    },
    "courses" : [ 
        "Python", 
        "React", 
        "PHP"
    ],
    "department" : "HR",
    "status" : "active"
},
{
    "firstName" : "Neil",
    "lastName" : "Armstrong",
    "age" : 82.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "neilarmstrong@gmail.com"
    },
    "courses" : [ 
        "React", 
        "Laravel", 
        "Sass"
    ],
    "department" : "HR",
    "status" : "active"
},
{
    "firstName" : "Bill",
    "lastName" : "Gates",
    "age" : 65.0,
    "contact" : {
        "phone" : "12345678",
        "email" : "bill@gmail.com"
    },
    "courses" : [ 
        "PHP", 
        "Laravel", 
        "HTML"
    ],
    "department" : "Operations",
    "status" : "active"
}
	])

// 2. find users with letter s in firstName and letter d in lastName
db.users.find(
	{$or:[{"firstName":{$regex:"s",$options:"i"}},
			{"lastName":{$regex:"d",$options:"i"}}]},
	{"firstName":1,
"lastName":1,
"_id":0}
	)

//3. Find users who are in HR Department and age is greater or equal to 70
db.users.find(
	{$and:[{"department":"HR"},
		{"age":{$gte:70}}]}
	)

//4. Find users with letter e in first name and age is less than or equal to 30
db.users.find(
	{$and:[
		{"firstName":{$regex:"e",$options:"i"}}
		,{"age":{$lte:30}}]}
	)